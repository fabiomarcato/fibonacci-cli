import NthFibonacci from "./fibonacci.js";

describe('Nth Fibbonacci tests', () => {
    test('10th fibonacci number should be 34', () => {
      const fibonacci = new NthFibonacci(10);
      expect(fibonacci.nthFibonacci()).toEqual(34);
    });  
});
