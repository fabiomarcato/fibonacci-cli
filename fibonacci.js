class NthFibonacci {
    constructor(nthNumber) {
      this.nthNumber = nthNumber
      this.nthFibonacciNumber = this.#nthFibonacci()
    }
  
    nthFibonacci() {
      return (this.nthFibonacciNumber);
    }
  
    #nthFibonacci() {
      var fibonacciArray = []
      fibonacciArray[0] = 0
      fibonacciArray[1] = 1
  
      if (this.nthNumber == 1){
        return 0
      }
  
      for (var i = 2; i < this.nthNumber; i++) {
        fibonacciArray[i] = fibonacciArray[i - 2] + fibonacciArray[i - 1]
      }
      return (fibonacciArray[fibonacciArray.length -1 ]);
    }
  }
  
  module.exports = NthFibonacci;
  