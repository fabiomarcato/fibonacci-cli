#!/usr/bin/env node

const yargs = require('yargs');
const NthFibonacci = require('./fibonacci.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <fibonacci number>")
   .option("n", { alias: "fibbo", describe: "Nth fibonacci number", type: "int", demandOption: true })
   .argv;

  const fibonacci = new NthFibonacci(options.n);
    
  console.log(fibonacci.nthFibonacci());
}


